# EMTF++ Neural Network

Original Author: Jia Fu Low

This repository is forked from 2 repositories that Jia Fu Low maintained. The first, the emtf-nnet, and the second, a
collection of Jupyter notebooks, in which training and studies are performed.

## Attributions

- This package borrows heavily codes from [TensorFlow](https://www.tensorflow.org/) and
  [Keras](https://keras.io/). Copyrights of the borrowed codes belong to their respective copyright owners.
- Forked from Jia Fu's repositories.

## License

This package is licensed under the terms of the Apache license. See [LICENSE](LICENSE) for more information.
