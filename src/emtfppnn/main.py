import argparse
import os
import sys

import matplotlib
import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt

from emtfppnn.commands.training import command as train_command
from emtfppnn.execution import runtime
from emtfppnn.execution.runtime import get_logger


def main():
    # PARSER
    parser = argparse.ArgumentParser(prog=runtime.project_name, description='EMTF++ Neural Network')

    parser.add_argument('--debug', dest='debug_en', action='store_const',
                        const=True, default=False,
                        help='Enable debug mode')

    parser.add_argument('--modelhome', '-m', dest='modelhome', default=None,
                        help='Directory where all the model\'s files are stored')

    subparsers = parser.add_subparsers(dest='parser', metavar="COMMAND", help='Command to be run')

    train_command.configure_parser(subparsers)

    args = parser.parse_args()

    # SET LOCAL
    runtime.model_root = args.modelhome

    # DEBUG MODE
    if args.debug_en:
        runtime.debug_en = True

    # PRINT BANNER
    print_banner()

    # RUN COMMAND
    if args.parser == 'train':
        train_command.run(args)


def print_banner():
    if sys.version_info[0] < 3:
        raise AssertionError('Please run this notebook with Python 3.')

    # Preamble (ML)
    np.random.seed(2027)  # set random seed
    tf.random.set_seed(2027)  # set random seed

    # Get plot style and color map
    plt.style.use(runtime.resource('tdrstyle.mplstyle'))

    # Get logger
    get_logger().info('Debug Mode       : {}'.format(runtime.debug_en))
    get_logger().info('Using CMSSW      : {}'.format(os.environ.get('CMSSW_VERSION', 'N/A')))
    get_logger().info('Using python     : {}'.format(sys.version.replace('\n', '')))
    get_logger().info('Using numpy      : {}'.format(np.__version__))
    get_logger().info('Using matplotlib : {}'.format(matplotlib.__version__))
    get_logger().info('Using tensorflow : {}'.format(tf.__version__))
    get_logger().info('Using keras      : {}'.format(tf.keras.__version__))
    get_logger().info('.. devices       : {}'.format(tf.config.list_physical_devices()))
