from abc import ABC
from abc import abstractmethod


class AbstractAnalyzer(ABC):

    def __init__(self):
        self.plotters = dict()
        self.consolidated = False

    def checkout_plotter(self, clazz, name, *args, **kwargs):
        plotter = self.plotters.get(name)

        if plotter is not None:
            return plotter

        plotter = clazz(name, *args, **kwargs)
        self.plotters[name] = plotter

        return plotter

    @abstractmethod
    def process(self, data):
        pass

    @abstractmethod
    def post_production(self):
        pass

    def consolidate(self):
        if self.consolidated:
            return

        for plotter in self.plotters.values():
            plotter.consolidate()

        self.post_production()

        self.consolidated = True

    def write(self):
        self.consolidate()

        for plotter in self.plotters.values():
            if plotter.write_en:
                plotter.write()


class AbstractPlotter(ABC):

    def __init__(self):
        self.write_en = True

    @abstractmethod
    def consolidate(self):
        pass

    @abstractmethod
    def write(self):
        pass
