import importlib
import os
import sys

if __name__ == '__main__':
    # READ LAUNCHER ENV VARIABLES
    project_home = os.environ['PROJECT_HOME']
    project_name = os.environ['PROJECT_NAME']
    project_main_pkg = os.environ['PROJECT_MAIN_PKG']

    # ADD PROJECT TO PATH
    sys.path.append(project_home + '/src')
    print(project_home)

    # RUNTIME
    from emtfppnn.execution import runtime

    runtime.project_root = project_home
    runtime.project_name = project_name

    # RUN MAIN
    main_module = importlib.import_module(project_main_pkg + '.main')
    main_module.main()
