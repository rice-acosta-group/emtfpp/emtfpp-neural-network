"""Numpy-related utilities."""

import numpy as np


def div_no_nan(x, y):
    x, y = np.asarray(x), np.asarray(y)

    # IF Y IS A SCALAR AND Y=0 RETURN A ZERO ARRAY WITH THE SHAPE OF X
    # ELSE RETURN X DIVIDED BY Y
    if y.ndim == 0:
        return (x - x) if y == 0 else (x / y)

    # MAKE SURE THAT X AND Y HAVE THE SAME SHAPE
    if x.ndim > 0 and x.shape != y.shape:
        raise ValueError('Inconsistent shapes: x.shape={0} y.shape={1}'.format(
            x.shape, y.shape))

    # TRUE DIVIDE
    mask = (y == 0)

    # MAKE ALL X WHERE Y IS 0, 0
    x = x - (x * mask)

    # MAKE ALL THE Y WHERE Y IS 0, 1
    y = y + 1.0 * mask

    return x / y
