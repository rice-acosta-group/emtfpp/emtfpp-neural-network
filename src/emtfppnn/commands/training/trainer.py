import contextlib
import os
import tempfile
from datetime import datetime

from matplotlib import pyplot as plt

from emtfppnn.execution.runtime import get_logger


class ModelTrainer(object):

    def __init__(self, model, log_dir='keras_logs', prefix='', suffix='.log'):
        self.model = model
        self.log_dir = log_dir
        self.prefix = prefix
        self.suffix = suffix

    def fit(self, *args, **kwargs):
        if not os.path.exists(self.log_dir):
            os.makedirs(self.log_dir)

        fd, name = tempfile.mkstemp(prefix=self.prefix, suffix=self.suffix, dir=self.log_dir, text=True)

        start_time = datetime.now()

        get_logger().info('Begin training ...')

        # Fit
        with os.fdopen(fd, 'w') as f:
            with contextlib.redirect_stdout(f):
                history = self.model.fit(*args, **kwargs)

        get_logger().info('Done training. Time elapsed: {}'.format(str(datetime.now() - start_time)))

        # Plot history
        HistoryPlotter(history).plot()

        return history


class HistoryPlotter(object):

    def __init__(self, history, metric=None):
        self.history = history

        if metric is None:
            metric = 'loss'

        self.metric = metric

    def plot(self):
        train_value = self.history.history[self.metric]
        val_value = self.history.history['val_' + self.metric]
        lr_value = self.history.history['lr']
        maxnorm_value = self.history.history['gradient_maxnorm']
        tup = (len(self.history.epoch), len(self.history.epoch), self.metric,
               train_value[-1], 'val_' + self.metric, val_value[-1])

        get_logger().info('Epoch {}/{} - {}: {:.4f} - {}: {:.4f}'.format(*tup))

        if len(self.history.epoch) > 10:
            fig, axs = plt.subplots(1, 2, figsize=(6, 6 / 2), tight_layout=True)
            ax = axs[0]
            ax.plot(self.history.epoch, lr_value, color='C0')
            ax.set_xlabel('Epochs')
            ax.set_ylabel('Learning rate')
            ax.grid(True)
            ax = axs[1]
            ax.plot(self.history.epoch, train_value, color='C0', label='Train')
            ax.plot(self.history.epoch, val_value, '--', color='C0', label='Val')
            ax.set_xlabel('Epochs')
            ax.set_ylabel(self.metric.replace('_', ' ').title())
            ax.set_ylim(0, 1.5)
            ax.legend(loc='upper right')
            ax.grid(True)
            plt.savefig("learning_rate.png")

            fig, axs = plt.subplots(1, 2, figsize=(6, 6 / 2), tight_layout=True)
            ax = axs[0]
            ax.plot(self.history.epoch, maxnorm_value, color='C0')
            ax.set_xlabel('Epochs')
            ax.set_ylabel('Gradient maxnorm')
            ax.set_ylim(0, 1.5)
            ax.grid(True)
            plt.savefig("grad_maxnorm.png")
