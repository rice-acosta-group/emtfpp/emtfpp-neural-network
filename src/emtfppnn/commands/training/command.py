import h5py
import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf
from matplotlib import pyplot as plt

from emtfppnn.commands.commons.tools import fit_gaus
from emtfppnn.commands.commons.tools import gaus
from emtfppnn.commands.commons.tools import get_colormap
from emtfppnn.commands.commons.tools import mean_absolute_error
from emtfppnn.commands.commons.tools import mean_squared_error
from emtfppnn.commands.commons.tools import median_absolute_deviation
from emtfppnn.commands.training.trainer import ModelTrainer
from emtfppnn.execution import runtime
from emtfppnn.execution.runtime import get_logger
from emtfppnn.keras.callbacks import LearningRateLogger
from emtfppnn.keras.sparsity import UpdatePruningStep
from emtfppnn.keras.utils import save_nnet_model
from emtfppnn.neural_network import endless_nnet_v3
from emtfppnn.neural_network.endless_nnet_v3 import create_sparsity_m_by_n_list


def configure_parser(subparsers):
    parser = subparsers.add_parser('train', help='Train NN')


def run(args):
    # Get color map
    cm = get_colormap()

    # Settings

    # zone: (0,1,2) -> eta=(1.98..2.5, 1.55..1.98, 1.2..1.55)
    zone = 0
    # zone = 1
    # zone = 2

    # timezone: (0,1,2) -> BX=(0,-1,-2)
    timezone = 0

    # maxevents
    maxevents = 10_000
    # maxevents = -1

    # NN pruning: True/False
    do_pruning = False

    # NN quantization: True/False
    do_quantization = False

    # Print Settings
    get_logger().info('Using settings   : |')
    get_logger().info('.. zone          : {}'.format(zone))
    get_logger().info('.. timezone      : {}'.format(timezone))
    get_logger().info('.. maxevents     : {}'.format(maxevents))

    # Load features and truths
    features_fname = runtime.model_resource('features_dxy.h5')

    get_logger().info('Loading from {}'.format(features_fname))

    with h5py.File(features_fname, 'r') as loaded:
        features = np.array(loaded['features'])
        truths = np.array(loaded['truths'])
        noises = np.array(loaded['noises'])

    get_logger().info('features: {} truths: {} noises: {}'.format(
        (features.shape, features.dtype.name),
        (truths.shape, truths.dtype.name),
        (noises.shape, noises.dtype.name)))

    tf.config.optimizer.set_jit(True)  # enable XLA
    # tf.config.threading.set_inter_op_parallelism_threads(32)
    # tf.config.threading.set_intra_op_parallelism_threads(32)

    # Split into train/test
    x_train, x_test, y_train, y_test = endless_nnet_v3.get_x_y_data(
        features, truths, batch_size=8192)

    get_logger().info('x_train: {} y_train: {} x_test: {} y_test: {}'.format(
        x_train.shape, y_train.shape, x_test.shape, y_test.shape))

    # Set hyperparameters
    learning_rate = 0.005
    final_learning_rate = learning_rate * 0.02
    gradient_clipnorm = 10.
    warmup_epochs = 30
    epochs = warmup_epochs + 120
    batch_size = 2048

    num_train_samples = x_train.shape[0]
    callbacks = [LearningRateLogger()]

    get_logger().info('learning_rate: {} final_learning_rate: {} epochs: {} batch_size: {}'.format(
        learning_rate, final_learning_rate, epochs, batch_size))

    # Create model
    preprocessing_layer = endless_nnet_v3.create_preprocessing_layer(x_train)

    regularization_layer = endless_nnet_v3.create_regularization_layer(noises, batch_size=batch_size)

    lr_schedule = endless_nnet_v3.create_lr_schedule(
        num_train_samples,
        epochs=epochs,
        warmup_epochs=warmup_epochs,
        batch_size=batch_size,
        learning_rate=learning_rate,
        final_learning_rate=final_learning_rate)

    optimizer = endless_nnet_v3.create_optimizer(
        lr_schedule,
        gradient_clipnorm=gradient_clipnorm)

    model = endless_nnet_v3.create_model(
        preprocessing_layer,
        regularization_layer,
        optimizer)

    model.summary()

    # Train model
    history = ModelTrainer(model).fit(
        x_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1,
        callbacks=callbacks, validation_data=(x_test, y_test), shuffle=True)

    # Save model
    base_model = model
    save_nnet_model(model)

    get_logger().info('Saved as {}'.format(model.name + '.h5'))

    # Create and train pruned model (1)
    if do_pruning:
        pruning_callbacks = callbacks + [UpdatePruningStep()]
        pruning_learning_rate = learning_rate * 0.2
        pruning_epochs = 10
        pruning_schedule = endless_nnet_v3.create_pruning_schedule(
            num_train_samples, epochs=pruning_epochs, batch_size=batch_size)

        lr_schedule = endless_nnet_v3.create_lr_schedule(
            num_train_samples, epochs=pruning_epochs, warmup_epochs=0, batch_size=batch_size,
            learning_rate=pruning_learning_rate, final_learning_rate=pruning_learning_rate)

        optimizer = endless_nnet_v3.create_optimizer(
            lr_schedule, gradient_clipnorm=gradient_clipnorm)

        # Pruning loop
        layers_to_prune = 'dense'
        layers_to_preserve = {'dense', 'dense_1', 'dense_2'}
        sparsity_m_by_n_list = endless_nnet_v3.create_sparsity_m_by_n_list(12, 40)

        for sparsity_m_by_n in sparsity_m_by_n_list:
            pruned_model = endless_nnet_v3.create_pruned_model(
                model, optimizer, layers_to_prune, layers_to_preserve,
                pruning_schedule=pruning_schedule, sparsity_m_by_n=sparsity_m_by_n)

            # pruned_model.summary()

            history = ModelTrainer(pruned_model).fit(
                x_train, y_train, batch_size=batch_size, epochs=pruning_epochs, verbose=1,
                callbacks=pruning_callbacks, validation_data=(x_test, y_test), shuffle=True)

            # print(history.history['sparsities'])

    # Create and train pruned model (2)
    if do_pruning:
        layers_to_prune = 'dense_1'
        layers_to_preserve = {'dense', 'dense_1', 'dense_2'}
        sparsity_m_by_n_list = create_sparsity_m_by_n_list(4, 28)

        for sparsity_m_by_n in sparsity_m_by_n_list:
            pruned_model = endless_nnet_v3.create_pruned_model(
                model, optimizer, layers_to_prune, layers_to_preserve,
                pruning_schedule=pruning_schedule, sparsity_m_by_n=sparsity_m_by_n)

            # pruned_model.summary()

            history = ModelTrainer(pruned_model).fit(
                x_train, y_train, batch_size=batch_size, epochs=pruning_epochs, verbose=1,
                callbacks=pruning_callbacks, validation_data=(x_test, y_test), shuffle=True)

            # print(history.history['sparsities'])

    # Create and train pruned model (3)
    if do_pruning:
        layers_to_prune = 'dense_2'
        layers_to_preserve = {'dense', 'dense_1', 'dense_2'}
        sparsity_m_by_n_list = endless_nnet_v3.create_sparsity_m_by_n_list(4, 24)

        for sparsity_m_by_n in sparsity_m_by_n_list:
            pruned_model = endless_nnet_v3.create_pruned_model(
                model, optimizer, layers_to_prune, layers_to_preserve,
                pruning_schedule=pruning_schedule, sparsity_m_by_n=sparsity_m_by_n)
            # pruned_model.summary()

            history = ModelTrainer(pruned_model).fit(
                x_train, y_train, batch_size=batch_size, epochs=pruning_epochs, verbose=1,
                callbacks=pruning_callbacks, validation_data=(x_test, y_test), shuffle=True)
            # print(history.history['sparsities'])

    if do_pruning:
        # Debug
        print('dense {}/{}'.format(
            np.count_nonzero(model.layers[1].weights[0]), model.layers[1].weights[0].numpy().size))

        print('dense_1 {}/{}'.format(
            np.count_nonzero(model.layers[4].weights[0]), model.layers[4].weights[0].numpy().size))

        print('dense_2 {}/{}'.format(
            np.count_nonzero(model.layers[7].weights[0]), model.layers[7].weights[0].numpy().size))

        print('dense_final {}/{}'.format(
            np.count_nonzero(model.layers[11].weights[0]), model.layers[11].weights[0].numpy().size))

    # Create quantized model
    if do_quantization:
        quant_learning_rate = learning_rate * 0.2
        quant_epochs = 200

        lr_schedule = endless_nnet_v3.create_lr_schedule(
            num_train_samples, epochs=quant_epochs, warmup_epochs=0, batch_size=batch_size,
            learning_rate=quant_learning_rate, final_learning_rate=final_learning_rate)

        optimizer = endless_nnet_v3.create_optimizer(
            lr_schedule, gradient_clipnorm=gradient_clipnorm)

        quant_model = endless_nnet_v3.create_quant_model(model, optimizer)
        quant_model.summary()

    # Train quantized model
    if do_quantization:
        history = ModelTrainer(quant_model).fit(
            x_train, y_train, batch_size=batch_size, epochs=quant_epochs, verbose=1,
            callbacks=callbacks, validation_data=(x_test, y_test), shuffle=True)

        # Save model
        base_model = model
        model = quant_model
        save_nnet_model(model)
        get_logger().info('Saved as {}'.format(model.name + '.h5'))

        # Debug
        print('dense {}/{}'.format(
            np.count_nonzero(model.layers[1].weights[0]), model.layers[1].weights[0].numpy().size))

        print('dense_1 {}/{}'.format(
            np.count_nonzero(model.layers[3].weights[0]), model.layers[3].weights[0].numpy().size))

        print('dense_2 {}/{}'.format(
            np.count_nonzero(model.layers[5].weights[0]), model.layers[5].weights[0].numpy().size))

        print('dense_final {}/{}'.format(
            np.count_nonzero(model.layers[8].weights[0]), model.layers[8].weights[0].numpy().size))

    # Make predictions
    y_test_true = y_test.copy()
    y_test_pred = model.predict(x_test, batch_size=8192)

    # Subsets of x_test, y_test_true, y_test_pred
    shuffle = True

    index_array = np.arange(x_test.shape[0])

    if shuffle:
        np.random.shuffle(index_array)

    x_test_1 = x_test[index_array[:maxevents]]
    y_test_true_1 = y_test_true[index_array[:maxevents]]
    y_test_pred_1 = y_test_pred[index_array[:maxevents]]

    # Plot predictions
    fig = plt.figure(figsize=(4.5, 4.5))

    xx = np.squeeze(y_test_true)
    yy = np.squeeze(y_test_pred)

    gs = plt.GridSpec(2, 2, width_ratios=(5, 1), height_ratios=(1, 5), hspace=0.1, wspace=0.1)
    ax = fig.add_subplot(gs[1, 0])
    ax_top = fig.add_subplot(gs[0, 0], sharex=ax)
    ax_right = fig.add_subplot(gs[1, 1], sharey=ax)

    ax.hist2d(xx, yy, bins=(120, 120), range=((-0.7, 0.7), (-0.7, 0.7)), vmin=1, cmap=cm.viridis_mod)
    ax.set_xlabel(r'true $q/p_{\mathrm{T}}$ [1/GeV]')
    ax.set_ylabel(r'pred $q/p_{\mathrm{T}}$ [1/GeV]')
    ax_top.hist(xx, bins=120, range=(-0.7, 0.7))
    ax_top.tick_params(labelbottom=False, labelleft=False)
    ax_right.hist(yy, bins=120, range=(-0.7, 0.7), orientation='horizontal')
    ax_right.tick_params(labelbottom=False, labelleft=False)

    plt.savefig('pred_vs_true_qop.png')

    # Plot predictions
    fig = plt.figure(figsize=(4.5, 4.5))

    xx = np.squeeze(y_test_true_1)
    yy = np.squeeze(y_test_pred_1)

    gs = plt.GridSpec(2, 2, width_ratios=(5, 1), height_ratios=(1, 5), hspace=0.1, wspace=0.1)
    ax = fig.add_subplot(gs[1, 0])
    ax_top = fig.add_subplot(gs[0, 0], sharex=ax)
    ax_right = fig.add_subplot(gs[1, 1], sharey=ax)

    df = pd.DataFrame({'x': xx, 'y': yy})
    sns.kdeplot(data=df, x='x', y='y', levels=5, bw_adjust=1, ax=ax)
    ax.set_xlabel(r'true $q/p_{\mathrm{T}}$ [1/GeV]')
    ax.set_ylabel(r'pred $q/p_{\mathrm{T}}$ [1/GeV]')
    sns.kdeplot(data=df, x='x', bw_adjust=1, ax=ax_top)
    ax_top.set_xlabel('')
    ax_top.set_ylabel('')
    ax_top.tick_params(labelbottom=False, labelleft=False)
    sns.kdeplot(data=df, y='y', bw_adjust=1, ax=ax_right)
    ax_right.set_xlabel('')
    ax_right.set_ylabel('')
    ax_right.tick_params(labelbottom=False, labelleft=False)

    plt.savefig('pred_vs_true_qop_subset.png')

    # Plot Delta(q/pt)
    fig, axs = plt.subplots(1, 2, figsize=(6, 3), tight_layout=True)

    ya = np.squeeze(y_test_true)
    yb = np.squeeze(y_test_pred)
    yy = yb - ya
    yy_core = yy[np.abs(yy) < 0.3]

    ax = axs[0]
    hist, edges, _ = ax.hist(yy, bins=120, range=(-0.3, 0.3), histtype='stepfilled', color='g', alpha=0.6)
    metrics = (yy_core.mean(), yy_core.std(),
               mean_squared_error(ya, yb),
               mean_absolute_error(ya, yb),
               median_absolute_deviation(ya, yb))
    popt = fit_gaus(hist, edges, mu=metrics[0], sig=metrics[1])
    xdata = (edges[1:] + edges[:-1]) / 2
    ydata = gaus(xdata, popt[0], popt[1], popt[2])

    ax.plot(xdata, ydata, color='g')
    ax.set_xlabel(r'$\Delta(q/p_{\mathrm{T}})_{\mathrm{pred-true}}$ [1/GeV]')
    ax.set_ylabel(r'entries')

    get_logger().info('mu, sig, mse, mae, mad: {0:.4e}, {1:.4f}, {2:.4f}, {3:.4f}, {4:.4f}'.format(*metrics))
    get_logger().info('gaus fit (a, mu, sig): {0:.2f}, {1:.4e}, {2:.4f}'.format(*popt))

    # Repeat with pt > 14 GeV
    sel = np.squeeze(np.reciprocal(np.abs(y_test_true)) > 14.)
    ya = ya[sel]
    yb = yb[sel]
    yy = yy[sel]
    yy_core = yy[np.abs(yy) < 0.1]

    ax = axs[1]
    hist, edges, _ = ax.hist(yy, bins=120, range=(-0.1, 0.1), histtype='stepfilled', color='g', alpha=0.6)
    metrics = (yy_core.mean(), yy_core.std(),
               mean_squared_error(ya, yb),
               mean_absolute_error(ya, yb),
               median_absolute_deviation(ya, yb))
    popt = fit_gaus(hist, edges, mu=metrics[0], sig=metrics[1])
    xdata = (edges[1:] + edges[:-1]) / 2
    ydata = gaus(xdata, popt[0], popt[1], popt[2])

    ax.plot(xdata, ydata, color='g')
    ax.set_xlabel(r'$\Delta(q/p_{\mathrm{T}})_{\mathrm{pred-true}}$ [1/GeV]')
    ax.set_ylabel(r'entries')

    get_logger().info('mu, sig, mse, mae, mad: {0:.4e}, {1:.4f}, {2:.4f}, {3:.4f}, {4:.4f}'.format(*metrics))
    get_logger().info('gaus fit (a, mu, sig): {0:.2f}, {1:.4e}, {2:.4f}'.format(*popt))

    plt.savefig('delta_qop.png')

    # Plot Delta(q/pt) / pt
    fig, axs = plt.subplots(1, 2, figsize=(6, 3), tight_layout=True)

    ya = np.squeeze(np.abs(y_test_true / y_test_true))
    yb = np.squeeze(np.abs(y_test_true / y_test_pred))
    yy = yb - ya
    yy_core = yy[np.abs(yy) < 1.5]

    ax = axs[0]
    hist, edges, _ = ax.hist(yy, bins=120, range=(-2, 2), histtype='stepfilled', color='g', alpha=0.6)
    metrics = (yy_core.mean(), yy_core.std(),
               mean_squared_error(ya, yb),
               mean_absolute_error(ya, yb),
               median_absolute_deviation(ya, yb))
    popt = fit_gaus(hist, edges, mu=metrics[0], sig=metrics[1])
    xdata = (edges[1:] + edges[:-1]) / 2
    ydata = gaus(xdata, popt[0], popt[1], popt[2])

    ax.plot(xdata, ydata, color='g')
    ax.set_xlabel(r'$\Delta(q/p_{\mathrm{T}})_{\mathrm{pred-true}}$ [1/GeV]')
    ax.set_ylabel(r'entries')

    get_logger().info('mu, sig, mse, mae, mad: {0:.4e}, {1:.4f}, {2:.4f}, {3:.4f}, {4:.4f}'.format(*metrics))
    get_logger().info('gaus fit (a, mu, sig): {0:.2f}, {1:.4e}, {2:.4f}'.format(*popt))

    # Repeat with pt > 14 GeV
    sel = np.squeeze(np.reciprocal(np.abs(y_test_true)) > 14.)
    ya = ya[sel]
    yb = yb[sel]
    yy = yy[sel]
    yy_core = yy[np.abs(yy) < 1.5]

    ax = axs[1]
    hist, edges, _ = ax.hist(yy, bins=120, range=(-2, 2), histtype='stepfilled', color='g', alpha=0.6)
    metrics = (yy_core.mean(), yy_core.std(),
               mean_squared_error(ya, yb),
               mean_absolute_error(ya, yb),
               median_absolute_deviation(ya, yb))
    popt = fit_gaus(hist, edges, mu=metrics[0], sig=metrics[1])
    xdata = (edges[1:] + edges[:-1]) / 2
    ydata = gaus(xdata, popt[0], popt[1], popt[2])

    ax.plot(xdata, ydata, color='g')
    ax.set_xlabel(r'$\Delta(q/p_{\mathrm{T}})_{\mathrm{pred-true}}$ [1/GeV]')
    ax.set_ylabel(r'entries')

    get_logger().info('mu, sig, mse, mae, mad: {0:.4e}, {1:.4f}, {2:.4f}, {3:.4f}, {4:.4f}'.format(*metrics))
    get_logger().info('gaus fit (a, mu, sig): {0:.2f}, {1:.4e}, {2:.4f}'.format(*popt))

    plt.savefig('delta_qop_lt1p5.png')

    # Plot correlations
    fig, axs = plt.subplots(2, 2, figsize=(6, 6), tight_layout=True)

    xx = np.squeeze(y_test_true)
    yy = np.squeeze(y_test_pred)
    ax = axs[0, 0]
    ax.hist2d(xx, yy, bins=(100, 100), range=((-0.5, 0.5), (-0.8, 0.8)), vmin=1, cmap=cm.viridis_mod)
    ax.set_xlabel(r'true $q/p_{\mathrm{T}}$ [1/GeV]')
    ax.set_ylabel(r'pred $q/p_{\mathrm{T}}$ [1/GeV]')

    yy = np.squeeze(y_test_pred - y_test_true)
    ax = axs[0, 1]
    ax.hist2d(xx, yy, bins=(100, 100), range=((-0.5, 0.5), (-0.4, 0.4)), vmin=1, cmap=cm.viridis_mod)
    ax.set_xlabel(r'true $q/p_{\mathrm{T}}$ [1/GeV]')
    ax.set_ylabel(r'$\Delta(q/p_{\mathrm{T}})_{\mathrm{pred-true}}$ [1/GeV]')

    with np.errstate(divide='ignore'):
        yy = np.squeeze((y_test_pred - y_test_true) / np.abs(y_test_true))

    ax = axs[1, 0]
    ax.hist2d(xx, yy, bins=(100, 100), range=((-0.5, 0.5), (-3, 3)), vmin=1, cmap=cm.viridis_mod)
    ax.set_xlabel(r'true $q/p_{\mathrm{T}}$ [1/GeV]')
    ax.set_ylabel(r'$\Delta(q/p_{\mathrm{T}})_{\mathrm{pred-true}} \cdot (q \cdot p_{\mathrm{T}})$')

    with np.errstate(divide='ignore'):
        yy = np.squeeze(np.abs(y_test_true / y_test_pred) - 1)

    ax = axs[1, 1]
    ax.hist2d(xx, yy, bins=(100, 100), range=((-0.5, 0.5), (-3, 3)), vmin=1, cmap=cm.viridis_mod)
    ax.set_xlabel(r'true $q/p_{\mathrm{T}}$ [1/GeV]')
    ax.set_ylabel(r'$\Delta(p_{\mathrm{T}})_{\mathrm{pred-true}} / p_{\mathrm{T}}$')

    plt.savefig('correlation.png')

    # resolution defined as Delta(pt) over pt (no charge), binned in 1/pt (also no charge)
    xx = np.squeeze(np.abs(y_test_true))
    yy = np.squeeze(np.abs(y_test_true / y_test_pred) - 1)

    nbinsx = 100
    xedges = np.linspace(0, 0.5, num=nbinsx + 1)
    inds = np.digitize(xx, xedges[1:])

    xx_pt = np.zeros(nbinsx, dtype=np.float32)
    yy_mu = np.zeros(nbinsx, dtype=np.float32)
    yy_sig = np.zeros(nbinsx, dtype=np.float32)
    yy_tail = np.zeros(nbinsx, dtype=np.float32)
    yy_cov = np.zeros(nbinsx, dtype=np.float32)

    cache = {}

    for i in range(nbinsx):
        xx_i = xx[inds == i]
        if len(xx_i) < 100:  # not enough stats
            continue

        pt = xx_i.mean()
        pt = 1.0 / pt

        yy_i = yy[inds == i]
        yy_i = yy_i[(-1.0 <= yy_i) & (yy_i <= 1.5)]
        mu, sig = yy_i.mean(), yy_i.std()
        assert np.abs(mu) < 1.0
        assert np.abs(sig) < 2.0

        hist, edges = np.histogram(yy_i, bins=160, range=(-2, 2))
        xdata = (edges[1:] + edges[:-1]) / 2

        try:
            popt = fit_gaus(hist, edges, mu=mu, sig=sig)
        except:
            popt = np.array([np.nan, np.nan, np.nan])

        # print(i, len(xx_i), mu, sig, pt, popt)

        ydata = gaus(xdata, popt[0], popt[1], popt[2])
        tail = hist - ydata  # check non-gaussian high tail
        tail[tail < 0] = 0  # ignore hist < gaus
        tail[xdata < popt[2]] = 0  # ignore low tail
        tail = tail.sum() / len(xx_i)

        pct = np.percentile(yy_i, (100 - 90), overwrite_input=True)  # find 10-percentile
        cov = -1 * pct / popt[2]
        # If truly gaussian, the required number of sigmas is 1.28155 for 90% coverage
        # print(scipy.special.erfinv(0.80) * np.sqrt(2))

        xx_pt[i] = pt
        yy_mu[i] = popt[1]
        yy_sig[i] = popt[2]
        yy_tail[i] = tail
        yy_cov[i] = cov

        if 14. < pt < 15.:
            cache[0] = (i, xx_i, yy_i)
        elif 20. < pt < 22.:
            cache[1] = (i, xx_i, yy_i)
        elif 25. < pt < 28.:
            cache[2] = (i, xx_i, yy_i)
        elif 28. < pt < 33.:
            cache[3] = (i, xx_i, yy_i)

    # Make better resolution plot
    fig, axs = plt.subplots(2, 4, figsize=(9, 9 / 2), tight_layout=True)

    for i in range(4):
        ax = axs[0, i]
        (ii, xx_i, yy_i) = cache[i]  # read from cache
        hist, edges, _ = ax.hist(yy_i, bins=160, range=(-2, 2), histtype='stepfilled', facecolor='g', alpha=0.6)
        popt = fit_gaus(hist, edges, mu=yy_mu[ii], sig=yy_mu[ii])
        xdata = (edges[1:] + edges[:-1]) / 2
        ydata = gaus(xdata, popt[0], popt[1], popt[2])

        tail = hist - ydata  # check non-gaussian high tail
        tail[tail < 0] = 0  # ignore hist < gaus
        tail[xdata < popt[2]] = 0  # ignore low tail

        ax.plot(xdata, ydata, color='g')
        ax.fill_between(xdata, ydata, ydata + tail, facecolor='y')
        ax.set_xlabel(r'$\Delta(p_{\mathrm{T}})_{\mathrm{pred-true}} / p_{\mathrm{T}}$')
        ax.set_ylabel(r'entries')

        get_logger().info('gaus fit (a, mu, sig): {0:.2f}, {1:.4e}, {2:.4f}'.format(*popt))

    ax = axs[1, 0]
    ax.scatter(xx_pt, yy_mu, s=12, color='g', alpha=0.6)
    ax.set_xlim(1, 80)
    ax.set_ylim(-0.25, 0.25)
    ax.set_xscale('log')
    ax.set_xticks(list(range(1, 10)) + list(range(10, 80, 10)))
    ax.set_xlabel(r'true $p_{\mathrm{T}}$ [GeV]')
    ax.set_ylabel(r'$\Delta(p_{\mathrm{T}}) / p_{\mathrm{T}}$ bias')

    ax = axs[1, 1]
    ax.scatter(xx_pt, yy_sig, s=12, color='g', alpha=0.6)
    ax.set_xlim(1, 80)
    ax.set_ylim(0, 0.5)
    ax.set_xscale('log')
    ax.set_xticks(list(range(1, 10)) + list(range(10, 80, 10)))
    ax.set_xlabel(r'true $p_{\mathrm{T}}$ [GeV]')
    ax.set_ylabel(r'$\Delta(p_{\mathrm{T}}) / p_{\mathrm{T}}$ resolution')

    ax = axs[1, 2]
    ax.scatter(xx_pt, yy_tail, s=12, color='g', alpha=0.6)
    ax.set_xlim(1, 80)
    ax.set_ylim(0, 0.3)
    ax.set_xscale('log')
    ax.set_xticks(list(range(1, 10)) + list(range(10, 80, 10)))
    ax.set_xlabel(r'true $p_{\mathrm{T}}$ [GeV]')
    ax.set_ylabel(r'high tail proxy')

    ax = axs[1, 3]
    ax.scatter(xx_pt, yy_cov, s=12, color='g', alpha=0.6)
    ax.set_xlim(1, 80)
    ax.set_ylim(0, 2)
    ax.set_xscale('log')
    ax.set_xticks(list(range(1, 10)) + list(range(10, 80, 10)))
    ax.set_xlabel(r'true $p_{\mathrm{T}}$ [GeV]')
    ax.set_ylabel(r'90% cov proxy')

    plt.savefig('res.png')

    # Check regularization loss
    def regularization_pt_output():
        regularization = model.get_layer('regularization')
        x = regularization.loss_fn._data
        y = regularization.loss_fn._model(x, training=False)
        l1, bias = regularization.l1, regularization.bias
        clip_value_min, clip_value_max = 0., 100.
        y_as_pt = tf.math.reciprocal_no_nan(tf.math.abs(y))
        new_y_as_pt = tf.clip_by_value(y_as_pt - bias, clip_value_min, clip_value_max)

        return np.squeeze(new_y_as_pt.numpy())

    fig, ax = plt.subplots()
    yy = regularization_pt_output()
    ax.hist(yy, bins=100, range=(0, 100), density=True)
    ax.set_yscale('log')
    plt.savefig('regloss.png')

    get_logger().info('regularization_pt_output: {}/{} = {:.5f}'.format(
        np.count_nonzero(yy), yy.shape[0], np.count_nonzero(yy) / yy.shape[0]))
